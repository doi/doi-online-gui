const patternEMail = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/i

export const url = anUrl => {
	let url

	if (anUrl) {
		try {
			url = new URL(anUrl)
		} catch (_) {
			return false
		}

		return url.protocol === "http:" || url.protocol === "https:"
	}

	return true
}

export const email = anEmail => {
	if (anEmail) {
		if (!patternEMail.test(anEmail)) {
			return false
		}
	}

	return true
}
