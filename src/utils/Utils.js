import i18n from '@/locales'
import moment from 'moment'
import { sanitizeUrl } from "@braintree/sanitize-url"

export default class Utils {
	static createNestedObject(base, path, value) {
		var lastName

		if (value !== undefined) {
			lastName = path.pop()
		} else {
			lastName = false
		}

		for (var i = 0; i < path.length; i++) {
			base = base[path[i]] = base[path[i]] || {}
		}

		if (lastName) {
			base = base[lastName] = value
		}

		return base
	}

	static getAttributeOrDefault(base, names, defValue) {
		var obj = base

		if (!obj) {
			return defValue
		}

		for (var idx = 0; idx < names.length; idx++) {
			var attrName = names[idx]

			if (idx + 1 == names.length) {
				if (obj[attrName] !== undefined) {
					return obj[attrName]
				}
			} else if (obj[attrName] && typeof obj[attrName] === "object") {
				obj = obj[attrName]
			} else {
				break
			}
		}

		return defValue
	}

	static deleteAttribute(base, names) {
		var obj = base

		if (!obj) {
			return
		}

		for (var idx = 0; idx < names.length; idx++) {
			var attrName = names[idx]

			if (idx + 1 == names.length) {
				if (obj[attrName] !== undefined) {
					delete obj[attrName]
					break
				}
			} else if (obj[attrName] && typeof obj[attrName] === "object") {
				obj = obj[attrName]
			} else {
				break
			}
		}
	}

	static setAttribteIfNotNull(base, attrName, value) {
		if (value) {
			base[attrName] = value
		} else {
			delete base[attrName]
		}
	}

	static clearEmptyNestedObjectsbase(base) {
		for (var attr in base) {
			if (!base[attr] || typeof base[attr] !== 'object') {
				continue
			}

			this.clearEmptyNestedObjects(base[attr])

			if (Object.keys(base[attr]).length === 0) {
				delete base[attr]
			}
		}
	}

	static isEmptyObject(anObject) {
		if (typeof (anObject) === 'object') {
			if (JSON.stringify(anObject) === '{}' || JSON.stringify(anObject) === '[]') {
				return true
			} else if (!anObject) {
				return true
			}

			return false
		} else if (typeof (anObject) === 'string') {
			if (!anObject.trim()) {
				return true
			}

			return false
		} else if (typeof (anObject) === 'undefined') {
			return true
		}

		return false
	}

	static encodeHtml(str) {
		return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
	}

	static sanitizeUrl(urlString) {
		return sanitizeUrl(urlString)
	}

	static convertFileSizeFromByte2Metric(size) {
		if (size >= 0) {
			size = Math.floor(size);
			if (size < 1000) {
				return size + ' B'
			}
			size = Math.floor(size / 1000);
			if (size < 1000) {
				return size + ' KB'
			}
			size = Math.floor(size / 1000);
			if (size < 1000) {
				return size + ' MB'
			}
			size = Math.floor(size / 1000);
			return size + ' GB'
		}
		return 'n.a.'
	}
}
