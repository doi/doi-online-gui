import Logger from 'js-logger'
import axios from 'axios'

class ServiceDiscovery {
	constructor() {
		this.services = [];
		this.serviceUrlsMap = [];
		this.isDevBuild = process.env.VUE_APP_VERSION == 'dev'

		this.services.push('app-services-v1')
		if (this.isDevBuild) {
			this.serviceUrlsMap['app-services-v1'] = [new RegExp(/^.*\//).exec(window.location.href) + 'app-services-v1/']
		} else {
			this.serviceUrlsMap['app-services-v1'] = [new RegExp(/^.*\//).exec(window.location.href) + 'services/v1/']
		}

		this.initComplete = this.discover(
			(response) => {
				for (const service of response.data['service-array']) {
					this.services.push(service.name)

					if (this.isDevBuild) {
						this.serviceUrlsMap[service.name] = [new RegExp(/^.*\//).exec(window.location.href) + service.name + '/']
					} else {
						this.serviceUrlsMap[service.name] = service['url-array']
					}

					Logger.debug("REST discover call result: " + service.name + ' = ' + service['url-array'])
				}
			},
			(error) => {
				Logger.warn("REST discover call error: " + error)
			}
		)
	}

	getRestBaseUrl = (serviceName) => {
		var serviceUrlsArray = this.serviceUrlsMap[serviceName]

		if (serviceUrlsArray && serviceUrlsArray.length > 0) {
			return serviceUrlsArray[Math.floor(Math.random() * Math.floor(serviceUrlsArray.length))]
		}

		return null
	}

	getAppServicesUrl() {
		return this.getRestBaseUrl('app-services-v1')
	}

	getPCMPrivateServicesUrl() {
		return this.getServiceUrl('pcm-private-services-v2')
	}
	
	getPCMOpenServicesUrl() {
		return this.getServiceUrl('pcm-open-services-v2')
	}

	getDOMServicesUrl() {
		return this.getServiceUrl('dom-services-v1')
	}

	getDOIOnlineServicesUrl() {
		return this.getServiceUrl('doi-online-services-v1')
	}

	getServiceUrl(serviceName) {
		return this.initComplete.then(() => {
			return this.getRestBaseUrl(serviceName)
		})
	}

	discover = (sucessMethod, failMethod) => {
		return axios.get(this.getAppServicesUrl() + 'app/discover', {
			headers: {
				'Access-Control-Allow-Origin': '*',
				'Content-Type': 'application/json'
			}
		}).then(sucessMethod).catch((error) => {
			this.handleRESTError(error, failMethod)
		})
	}

	handleRESTError = (error, failMethod) => {
		// Local error handling with first priority
		if (failMethod) {
			if (failMethod(error)) {
				return
			}
		}

		// If local error handling allowed it, then
		// logout graceful
		let msgId
		let restBaseUrl = new RegExp(/^.*\//).exec(window.location.href)

		// For DEV environment, do nothing, otherwise logout
		if (process.env.VUE_APP_VERSION == 'dev') {
			if (error.request) {
				Logger.error('REST error occurred at "' + error.request.responseURL + '": ' + error.message)
			} else {
				Logger.error('REST error occurred: ' + error.message)
			}

			if (error.response) {
				Logger.error('REST response was: ', error.response.data)
				Logger.error('REST response headers were: ', error.response.headers)
				Logger.error('REST error stacktrace: ', error.stack)
			}
		} else {
			if (error.response) {
				// Request was made and server responded with status other than 2xx
				msgId = error.response.data['message-id']
			} else if (error.request) {
				// Request failed before submission
				msgId = error.message
			} else {
				// Request setup failed
				msgId = error.message
			}

			if (msgId) {
				window.location.href = restBaseUrl + 'logout.view?msg=' + msgId
			} else {
				window.location.href = restBaseUrl + 'logout.view?status=' + error.response.status
			}
		}
	}
}

const serviceDiscovery = new ServiceDiscovery()
Object.freeze(serviceDiscovery)
export default serviceDiscovery
