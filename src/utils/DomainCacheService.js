import Logger from 'js-logger'
import store from '@/store'
import DomainsService from '@/services/DomainsService'

export default class DomainCacheService {
	static loadDomain(language, domainName, callback) {
		if (!this.prepareCacheIfNotYetLoaded(domainName, callback)) {
			new DomainsService().getDomainData(
				domainName, true, language,
				(response) => {
					let data = []

					if (domainName === 'NSP_SEMESTER') {
						response.data['domain-value-array'].forEach(
							nspSemester => {
								if (nspSemester && nspSemester.textWert && nspSemester.textWert.trim() === 'DS') {
									// Skip (for DGM's sake)
								} else {
									data.push(nspSemester)
								}
							}
						)
					} else {
						data = response.data['domain-value-array']
					}

					this.addToCache(domainName, data)
				},
				(error) => {
					this.resetCache(domainName, error)
				}
			)
		}
	}

	static loadDOIDomain(language, domainName, callback) {
		if (!this.prepareCacheIfNotYetLoaded(domainName, callback)) {
			new DomainsService().getDOIDomainData(
				language, domainName,
				(response) => {
					let data = response.data['domain-value-array']
					this.addToCache(domainName, data)
				},
				(error) => {
					this.resetCache(domainName, error)
				}
			)
		}
	}

	static loadAreas(callback) {
		let domainName = 'DOM_AREAS'

		if (!this.prepareCacheIfNotYetLoaded(domainName, callback)) {
			new DomainsService().getAllAreas(
				(response) => {
					let data = response.data['area-array']
					this.addToCache(domainName, data)
				},
				(error) => {
					this.resetCache(domainName, error)
				}
			)
		}
	}

	static loadBuildings(callback) {
		let domainName = 'DOM_BUILDINGS'

		if (!this.prepareCacheIfNotYetLoaded(domainName, callback)) {
			new DomainsService().getAllBuildings(
				(response) => {
					let data = response.data['building-array']
					this.addToCache(domainName, data)
				},
				(error) => {
					this.resetCache(domainName, error)
				}
			)
		}
	}

	static addToCache(domainName, domainData) {
		const domState = store.getters.getDomainState
		const listeners = domState[domainName].requestors

		store.commit('addToDomainCache', {
			domainName: domainName,
			domainTable: domainData
		})

		listeners.forEach(callback => {
			if (callback) {
				callback(domainData)
			}
		})

		Logger.debug('REST Utils.loadDomain(' + domainName + ') loaded.')
	}

	static resetCache(domainName, error) {
		store.commit('setDomainState', {
			domainName: domainName,
			isLoading: false,
			requestors: []
		})

		Logger.warn('REST Utils.loadDomain(' + domainName + ') ERROR: ' + error)
	}

	static prepareCacheIfNotYetLoaded(domainName, callback) {
		const domainState = store.getters.getDomainState

		if (!domainState || !domainState[domainName]) {
			store.commit('setDomainState', {
				domainName: domainName,
				isLoading: true,
				requestors: [callback]
			})
		} else if (domainState[domainName].isLoading) {
			domainState[domainName].requestors.push(callback)
			store.commit('setDomainState', domainState[domainName])
			return true
		} else if (callback) {
			callback(this.getDomainData(domainName))
			return true
		}

		return false
	}

	static getDomainData(domainName) {
		let domValArray = store.state.domainCache[domainName]
		if (domValArray) {
			return domValArray
		}
		return []
	}

	static getDomainValDesc(domainName, wert, isLangBeschr) {
		let desc = ''
		let domainValArray = store.state.domainCache[domainName]
		if (domainValArray) {
			const domainValFound = domainValArray.find(
				domainVal => {
					if (domainVal.zahlWert) {
						return domainVal.zahlWert === wert
					} else if (domainVal.textWert) {
						return domainVal.textWert === wert
					}
					return false
				}
			)
			if (domainValFound) {
				if (isLangBeschr) {
					desc = domainValFound.textLang
				} else {
					desc = domainValFound.textKurz
				}
			}
		}
		return desc
	}
}
