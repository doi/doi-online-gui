export default class FullISO8601Parser {
    static ISO8601_PATTERN = "^([\\+-]?[0-9u]{4}(?!\\d{2}\\b))((-?)((0[1-9]|1[0-2])(\\3([12]\\d|0[1-9]|3[01]))?|W([0-4]\\d|5[0-2])(-?[1-7])?|(00[1-9]|0[1-9]\\d|[12]\\d{2}|3([0-5]\\d|6[1-6])))([T\\s]((([01]\\d|2[0-3])((:?)[0-5]\\d)?|24\\:?00)([\\.,]\\d+(?!:))?)?(\\17[0-5]\\d([\\.,]\\d+)?)?([zZ]|([\\+-])([01]\\d|2[0-3]):?([0-5]\\d)?)?)?)?$"

    static checkDates(iso8601Array) {
        for(let idx in iso8601Array) {
            if (!this.isValid(iso8601Array[idx])) {
                return false
            }
        }

        return true
    }

    static isValid(iso8601String) {
        if (!iso8601String) {
            return false
        }

        let rangeDelimiter = '/'
        let delimiterCount = iso8601String.split('/').length - 1
        if (delimiterCount > 1) {
            return false
        }

        if (delimiterCount == 0) {
            rangeDelimiter = '\n'
            delimiterCount = iso8601String.split('\n').length - 1

            if (delimiterCount == 0) {
                return this.parseDateTime(iso8601String.trim())
            } else if (delimiterCount > 1) {
                return false
            }
        }

        if (iso8601String.trim().startsWith(rangeDelimiter)) {
            return this.parseDateTime(iso8601String.trim()).getMoment()
        } else if (iso8601String.trim().endsWith(rangeDelimiter)) {
            return this.parseDateTime(iso8601String.trim()).getMoment()
        }

        let dateTimes = iso8601String.split(rangeDelimiter)
        return this.parseDateTime(dateTimes[0].trim()) && this.parseDateTime(dateTimes[1].trim())
    }

    static parseDateTime(dateTimeString) {
        let groups = dateTimeString.match(this.ISO8601_PATTERN)

        if (groups && groups.length > 0) {
            if (groups.length >= 1 && groups[1] != null) {
                if (!this.isNumeric(groups[1])) {
                    let lowerYearBorder = groups[1].replaceAll("u", "0")

                    if (!this.isNumeric(lowerYearBorder)) {
                        return false
                    }
                }
            } else {
                return false
            }

            if (groups.length >= 5 && groups[5] != null) {
                if (!this.isNumeric(groups[5])) {
                    return false
                }
            }

            if (groups.length >= 7 && groups[7] != null) {
                if (!this.isNumeric(groups[7])) {
                    return false
                }
            }

            if (groups.length >= 15 && groups[15] != null) {
                if (!this.isNumeric(groups[15])) {
                    return false
                }
            }

            if (groups.length >= 16 && groups[16] != null) {
                if (!this.isNumeric(groups[16])) {
                    return false
                }
            }

            if (groups.length >= 19 && groups[19] != null) {
                if (!this.isNumeric(groups[19])) {
                    return false
                }
            }

            if (groups.length >= 23 && groups[23] != null) {
                if (!this.isNumeric(groups[23])) {
                    return false
                }
            }

            if (groups.length >= 24 && groups[24] != null) {
                if (!this.isNumeric(groups[24])) {
                    return false
                }
            }

            return true
        }

        return false
    }

    static isNumeric(str) {
        if (typeof str != "string") {
            return false
        }

        return !isNaN(str) && 
               !isNaN(parseFloat(str))
    }

    static count(str, letter) {
        let count = 0

        for (let i = 0; i < str.length; i++) {
            if (str.charAt(i) == letter) {
                count += 1
            }
        }

        return count
    }
}
