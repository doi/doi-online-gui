import router from '@/router'

class BreadcrumbManager {
	constructor() {
		this.breadcrumbStack = []
	}

	setRootCrumb(crumb) {
		this.breadcrumbStack = []
		this.breadcrumbStack.push({
			priority: 0,
			index: 0,
			titleId: crumb.titleId,
			routeName: crumb.route.name,
			routeParams: crumb.route.params,
			routeQuery:  crumb.route.query,
			onClickFx:  crumb.onClickFx,
		})
	}

	getStack() {
		return this.breadcrumbStack
	}

	onClickBack() {
		if (this.breadcrumbStack.length > 1) {
			this.onClickCrumb(this.breadcrumbStack.length - 2)
		}
	}

	onClickCrumb(crumbIndex) {
		if (crumbIndex < this.breadcrumbStack.length) {
			let crumb = this.breadcrumbStack[crumbIndex]
			this.breadcrumbStack.splice(crumbIndex + 1)

			if (crumb.onClickFx) {
				crumb.onClickFx(crumb)
			}
			if (crumb.routeName) {
				if (router.currentRoute.value.name !== crumb.routeName) {
					router.push({
						name: crumb.routeName,
						params: crumb.routeParams,
						query: crumb.routeQuery
					})
				}
			}
		}
	}

	updateRoute(newRoute) {
		this.breadcrumbStack[this.breadcrumbStack.length - 1].route = newRoute
	}

	onNavigatedTo(newCrumb) {
		if (newCrumb && newCrumb.titleId) {
			this.breadcrumbStack = this.breadcrumbStack.filter((crumb) => {
				return crumb.priority < newCrumb.priority
			})

			this.breadcrumbStack.push({
				priority: newCrumb.priority,
				index: this.breadcrumbStack.length,
				titleId: newCrumb.titleId,
				routeName: newCrumb.route.value.name,
				routeParams: newCrumb.route.value.params,
				routeQuery:  newCrumb.route.value.query,
				onClickFx: newCrumb.onClickFx,
			})
		}
	}
}

export default BreadcrumbManager
