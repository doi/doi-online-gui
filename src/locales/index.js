import { createI18n } from 'vue-i18n/index'
import localeEN from './en.json'
import localeDE from './de.json'


export default createI18n({
	locale: 'de',
	fallbackLocale: 'de',
	messages: {
		en: localeEN,
		de: localeDE
	},
	numberFormats: {
		'en': {
			integer: {
				format: 'N',
				minimumFractionDigits: 0,
				useGrouping: false
			}
		},
		'de': {
			integer: {
				format: 'N',
				minimumFractionDigits: 0,
				useGrouping: true
			}
		}
	},
	datetimeFormats: {
		'en': {
			short: {
				year: 'numeric',
				month: '2-digit',
				day: '2-digit'
			},
			long: {
				year: 'numeric',
				month: 'short',
				day: 'numeric',
				hour: 'numeric',
				minute: 'numeric',
				hour12: false
			}
		},
		'de': {
			short: {
				year: 'numeric',
				month: '2-digit',
				day: '2-digit'
			},
			long: {
				year: 'numeric',
				month: 'short',
				day: 'numeric',
				hour: 'numeric',
				minute: 'numeric',
				hour12: false
			}
		}
	}
})
