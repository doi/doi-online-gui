import { createStore } from 'vuex'
import i18n from '@/locales'
import moment from 'moment'
import router from '@/router'
import Logger from 'js-logger'
import AppService from '@/services/AppService'
import BreadcrumbManager from '@/utils/BreadcrumbManager'


export default createStore({
	state: {
		appService: new AppService(),

		languages: [
			{
				short: 'en',
				long: 'English'
			},
			{
				short: 'de',
				long: 'Deutsch'
			}
		],

		currentLanguage: 'de',
		currentUser: null,
		isRoleChangeable: false,
		appVersion: process.env.VUE_APP_VERSION,
		appStage: process.env.VUE_APP_STAGE,
		appServerName: null,
		appBuildDate: process.env.VUE_APP_BUILDDATE,
		appBuildNumber: process.env.VUE_APP_BUILDNUMBER,
		appBuildBranch: process.env.VUE_APP_BRANCH,
		appHelloUrl: '#',

		breadcrumbStack: [],

		domainCache: [],
		domainState: [],

		objectStore: {}
	},

	getters: {
		getCurrentUser: state => {
			return state.currentUser
		},
		getAppVersion: state => {
			return state.appVersion
		},
		getAppStage: state => {
			return state.appStage
		},
		getAppBuildDate: state => {
			return state.appBuildDate
		},
		getAppBuildNumber: state => {
			return state.appBuildNumber
		},
		getAppBuildBranch: state => {
			return state.appBuildBranch
		},
		getAppHelloUrl: state => {
			return state.appHelloUrl
		},
		getAppServerName: state => {
			return state.appServerName
		},
		getCurrentLanguage: state => {
			if (!state.currentLanguage) {
				return state.languages[0].short
			} else {
				return state.currentLanguage
			}
		},
		getOtherLanguage: state => {
			if (state.currentLanguage == state.languages[0].short) {
				return state.languages[1].short
			}

			return state.languages[0].short
		},
		getLanguages: state => {
			return state.languages;
		},
		getDomainCache: state => {
			return state.domainCache
		},
		getDomainState: state => {
			return state.domainState
		},
		getBreadcrumbStack: state => {
			return state.breadcrumbStack
		},
		isUserInRoleUser: state => {
			if (state.currentUser) {
				return state.currentUser.grantedAuthorities.find(role => role.authority === 'DOI-Role-User')
			}

			return false
		},
		isUserInRoleAdmin: state => {
			if (state.currentUser) {
				return state.currentUser.grantedAuthorities.find(role => role.authority === 'DOI-Role-Admin')
			}

			return false
		},
		getUserRoles: state => {
			let roleAuthorities = []
			if (state.currentUser) {
				state.currentUser.grantedAuthorities.forEach(role => roleAuthorities.push(role.authority))
			}

			return roleAuthorities
		},
		isRoleChangeable: state => {
			return state.isRoleChangeable
		},
		getObjectStore: state => {
			return state.objectStore
		},
	},

	mutations: {
		setCurrentUser(state, currentUser) {
			state.currentUser = currentUser

			if (currentUser) {
				Logger.info("Signed on user is: " +
					currentUser.givenName + " " + currentUser.surname + "(" + currentUser.persId + ")")
			} else {
				Logger.info("Anonymous user")
			}
		},
		setRoleChangeable(state, isChangeable) {
			state.isRoleChangeable = isChangeable
		},
		setCurrentLanguage(state, lang) {
			if (state.currentLanguage != lang) {
				state.currentLanguage = lang
				state.domainCache = []
				state.domainState = []
				i18n.global.locale = lang
				moment.locale(lang)
				Logger.info("Application has changed to " + lang)
			}
		},
		setVersionAndBuild(state, serverVersion) {
			state.appHelloUrl = state.appService.getHelloUrl()
			state.appBuildNumber = process.env.VUE_APP_BUILDNUMBER.substring(0, 8)
			
			if (serverVersion) {
				if (serverVersion.serverName) {
					state.appServerName = serverVersion.serverName
				}
				if (process.env.VUE_APP_STAGE === serverVersion.devStage || !process.env.VUE_APP_STAGE) {
					state.appStage = serverVersion.devStage
				} else if (process.env.VUE_APP_STAGE === serverVersion.devStage || !serverVersion.devStage) {
					state.appStage = process.env.VUE_APP_STAGE
				} else {
					state.appStage = process.env.VUE_APP_STAGE + '/' + serverVersion.devStage
				}
			}
		},
		toggleLanguage(state) {
			var newLang = this.getters.getOtherLanguage
			state.currentLanguage = newLang
			state.domainCache = []
			state.domainState = []
			i18n.global.locale = newLang
			Logger.info("Application has swiched to " + newLang)
		},
		addToDomainCache(state, domain) {
			state.domainCache[domain.domainName] = domain.domainTable
			state.domainState[domain.domainName] = {
				isLoading: false,
				domainListener: []
			}
		},
		setDomainState(state, domainState) {
			state.domainState[domainState.domainName] = domainState
		},
		setBreadcrumbStack(state, newBreadcrumbStack) {
			state.breadcrumbStack = newBreadcrumbStack
		},
		addToObjectStore(state, object) {
			state.objectStore[object.key] = object.value
		},
		clearFromObjectStore(state, key) {
			state.objectStore[key] = null
		},
	},

	actions: {
		getAppContext(context, doLoadUserProfile) {
			if (doLoadUserProfile === 'true') {
				context.state.appService.whoAmI(
					(response) => {
						context.commit('setCurrentUser', response.data)
						context.commit('setVersionAndBuild', response.data.serverVersion)

						if (response.data.language) {
							context.commit('setCurrentLanguage', response.data.language)
						} else {
							if (router.history.current.query && router.history.current.query.lang) {
								context.commit('setCurrentLanguage', router.history.current.query.lang)
							} else {
								if (navigator.language && navigator.language.startsWith('de')) {
									context.commit('setCurrentLanguage', 'de')
								} else {
									context.commit('setCurrentLanguage', 'en')
								}
							}
						}
						Logger.debug("REST WhoAmI call result: " + JSON.stringify(response.data))
					},
					(error) => {
						context.commit('setCurrentUser', null)
						Logger.warn("REST WhoAmI call error: " + error)
					}
				)
			} else {
				context.commit('setVersionAndBuild', null)

				if (navigator.language && navigator.language.startsWith('de')) {
					context.commit('setCurrentLanguage', 'de')
				} else {
					context.commit('setCurrentLanguage', 'en')
				}
			}
		},
		initBreadcrumb(context, rootCrumb) {
			var breadcrumbManager = new BreadcrumbManager()
			breadcrumbManager.setRootCrumb(rootCrumb)

			context.commit('addToObjectStore', {
				key: 'breadcrumbManager',
				value: breadcrumbManager
			})
		}
	}
});
