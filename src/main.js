import { createApp } from 'vue'
import FloatingVue from 'floating-vue'
import 'floating-vue/dist/style.css'
import VueLoading from 'vue-loading-overlay'
import 'vue-loading-overlay/dist/vue-loading.css'
import { defineCustomElements } from '@duetds/date-picker/dist/loader'
import '@duetds/date-picker/dist/duet/themes/default.css'
import {registerErrorHandler} from '@/services/AppService'
import AppMain from './AppMain'
import store from '@/store'
import router from '@/router'
import Logger from 'js-logger'
import i18n from '@/locales'

Logger.useDefaults()
if (process.env.VUE_APP_VERSION != 'dev') {
	Logger.setLevel(Logger.WARN)
} else {
	Logger.setLevel(Logger.TRACE)
}

// make the tooltip appear immediately on hover
FloatingVue.options.themes.tooltip.delay.show = 0

const myApp = createApp(AppMain)

myApp.use(store)
	.use(router)
	.use(i18n)
	.use(FloatingVue)
	.use(VueLoading)
	.mount('#app')

defineCustomElements(window)
registerErrorHandler(myApp)