import i18n from '@/locales'
import moment from 'moment'

export default {
	methods: {
		getLocalizedDateStringFromDate(selectedDate) {
			if (isNaN(selectedDate)) {
				return ''
			}
			const aMoment = moment(selectedDate.getTime())
			return aMoment.format(i18n.global.t('global.dates.dateformat'))
		},
		
		getLocalizedDateTimeStringFromDate(selectedDate) {
			if (!selectedDate) {
				return ''
			}

			const aMoment = moment(selectedDate.getTime())
			return aMoment.format(i18n.global.t('global.dates.datetimeformat'))
		},

		getLocalizedDateTimeStringWithSecFromDate(selectedDate) {
			if (!selectedDate) {
				return ''
			}

			const aMoment = moment(selectedDate.getTime())
			return aMoment.format(i18n.global.t('global.dates.datetimeformatlong'))
		},

		getDateFromLocalizedDateString(dateString) {
			if (dateString && dateString.trim().length > 0) {
				var selectedDate = moment(dateString.trim(), i18n.global.t('global.dates.dateformat')).toDate()
				return selectedDate
			}
			return null
		},

		getDateFromISODateString(isoDateString) {
			if (!isoDateString) {
				return null
			}

			return moment(isoDateString, 'YYYY-MM-DD').toDate()
		},

		getDateFromISODateTimeString(isoDateTimeString) {
			if (!isoDateTimeString) {
				return null
			}

			return moment(isoDateTimeString, 'YYYY-MM-DD HH:mm:ss').toDate()
		},

		getLocalizedDateStringFromMillis(timeInMs) {
			const d = new Date(timeInMs)
			if (isNaN(d)) {
				return 'n/a'
			}
			const aMoment = moment(timeInMs)
			return aMoment.format(i18n.global.t('global.dates.dateformat'))
		},

		getISODateStringFromDate(selectedDate) {
			if (!selectedDate) {
				return null
			}
			if (isNaN(selectedDate)) {
				return 'n/a'
			}
			const aMoment = moment(selectedDate)
			return aMoment.format('YYYY-MM-DD')		
		},

		getISODateTimeStringFromDate(selectedDate) {
			if (isNaN(selectedDate)) {
				return 'n/a'
			}
			const aMoment = moment(selectedDate)
			return aMoment.format('YYYY-MM-DD HH:mm:ss')
		}
	
	}
}