import axios from 'axios'
import serviceDiscovery from '@/utils/ServiceDiscovery'

export default class DomainsService {
	constructor() {
		this.serviceDiscovery = serviceDiscovery
	}

	getServiceUrl = ((serviceCall) => {
		serviceDiscovery.getDOMServicesUrl().then((serviceUrl) => {
			serviceCall(serviceUrl)
		})
	})

	getDOIServiceUrl = ((serviceCall) => {
		serviceDiscovery.getDOIOnlineServicesUrl().then((serviceUrl) => {
			serviceCall(serviceUrl)
		})
	})

	getDomainData = (domainName, inclInactive, lang, sucessMethod, failMethod) => {
		let self = this
		this.getServiceUrl((serviceUrl) => {
			return axios.get(serviceUrl + 'domains/' + domainName, {
				headers: {
					'Access-Control-Allow-Origin': '*',
					'Content-Type': 'application/json'
				},
				params: {
					"lang": lang,
					"all": inclInactive
				}
			}).then(sucessMethod).catch(function(error) {
				self.serviceDiscovery.handleRESTError(error, failMethod)
			})
		})

	}

	searchDomain = (domainName, inclInactive, lang, searchTerm, isSearchShortText, sucessMethod, failMethod) => {
		let self = this
		this.getServiceUrl((serviceUrl) => {
			return axios.get(serviceUrl + 'domains/' + domainName, {
				headers: {
					'Access-Control-Allow-Origin': '*',
					'Content-Type': 'application/json'
				},
				params: {
					"lang": lang,
					"all": inclInactive,
					"searchTerm": searchTerm,
					"searchShortText": isSearchShortText
				}
			}).then(sucessMethod).catch(function(error) {
				self.serviceDiscovery.handleRESTError(error, failMethod)
			})
		})
	}

	getTextForValue = (domainName, value, lang, sucessMethod, failMethod) => {
		let self = this
		this.getServiceUrl((serviceUrl) => {
			return axios.get(serviceUrl + 'domains/' + domainName + '/values/' + value, {
				headers: {
					'Access-Control-Allow-Origin': '*',
					'Content-Type': 'application/json'
				},
				params: {
					"lang": lang
				}
			}).then(sucessMethod).catch(function(error) {
				self.serviceDiscovery.handleRESTError(error, failMethod)
			})
		})
	}

	getValueForText = (domainName, text, isSearchShortText, lang, sucessMethod, failMethod) => {
		let self = this
		this.getServiceUrl((serviceUrl) => {
			return axios.get(serviceUrl + 'domains/' + domainName + '/texts/' + text, {
				headers: {
					'Access-Control-Allow-Origin': '*',
					'Content-Type': 'application/json'
				},
				params: {
					"lang": lang,
					"searchShortText": isSearchShortText
				}
			}).then(sucessMethod).catch(function(error) {
				self.serviceDiscovery.handleRESTError(error, failMethod)
			})
		})
	}

	getAllAreas = (sucessMethod, failMethod) => {
		let self = this
		this.getServiceUrl((serviceUrl) => {
			return axios.get(serviceUrl + 'locations/areas', {
				headers: {
					'Access-Control-Allow-Origin': '*',
					'Content-Type': 'application/json'
				}
			}).then(sucessMethod).catch(function(error) {
				self.serviceDiscovery.handleRESTError(error, failMethod)
			})
		})
	}

	getAllBuildings = (sucessMethod, failMethod) => {
		let self = this
		this.getServiceUrl((serviceUrl) => {
			return axios.get(serviceUrl + 'locations/buildings', {
				headers: {
					'Access-Control-Allow-Origin': '*',
					'Content-Type': 'application/json'
				}
			}).then(sucessMethod).catch(function(error) {
				self.serviceDiscovery.handleRESTError(error, failMethod)
			})
		})
	}

	getBuildingsForArea = (area, sucessMethod, failMethod) => {
		let self = this
		this.getServiceUrl((serviceUrl) => {
			return axios.get(serviceUrl + 'locations/areas/' + area + '/buildings', {
				headers: {
					'Access-Control-Allow-Origin': '*',
					'Content-Type': 'application/json'
				}
			}).then(sucessMethod).catch(function(error) {
				self.serviceDiscovery.handleRESTError(error, failMethod)
			})
		})
	}

	getFloorsForBuilding = (building, sucessMethod, failMethod) => {
		let self = this
		this.getServiceUrl((serviceUrl) => {
			return axios.get(serviceUrl + 'locations/buildings/' + building + '/floors', {
				headers: {
					'Access-Control-Allow-Origin': '*',
					'Content-Type': 'application/json'
				}
			}).then(sucessMethod).catch(function(error) {
				self.serviceDiscovery.handleRESTError(error, failMethod)
			})
		})
	}

	getRoomsForBuildingAndFloor = (building, floor, sucessMethod, failMethod) => {
		let self = this
		this.getServiceUrl((serviceUrl) => {
			return axios.get(serviceUrl + 'locations/buildings/' + building + '/floors/' + floor + '/rooms', {
				headers: {
					'Access-Control-Allow-Origin': '*',
					'Content-Type': 'application/json'
				}
			}).then(sucessMethod).catch(function(error) {
				self.serviceDiscovery.handleRESTError(error, failMethod)
			})
		})
	}

	getSchools = (lang, searchTerm, schoolTypes, inclInactive, rsSize, rsFirst, sucessMethod, failMethod) => {
		let self = this
		this.getServiceUrl((serviceUrl) => {
			return axios.get(serviceUrl + 'schools', {
				headers: {
					'Access-Control-Allow-Origin': '*',
					'Content-Type': 'application/json'
				},
				params: {
					"lang": lang,
					"inactive-incl": inclInactive,
					"search-term": searchTerm,
					"school-types": schoolTypes,
					"rs-size": rsSize,
					"rs-first": rsFirst
				}
			}).then(sucessMethod).catch(function(error) {
				self.serviceDiscovery.handleRESTError(error, failMethod)
			})
		})
	}

	getSchoolById = (lang, id, sucessMethod, failMethod) => {
		let self = this
		this.getServiceUrl((serviceUrl) => {
			return axios.get(serviceUrl + 'schools/' + id, {
				headers: {
					'Access-Control-Allow-Origin': '*',
					'Content-Type': 'application/json'
				},
				params: {
					"lang": lang
				}
			}).then(sucessMethod).catch(function(error) {
				self.serviceDiscovery.handleRESTError(error, failMethod)
			})
		})
	}

	searchOrgEinheitByFilter = (lang, searchTerm, guideNumber, outType, extInfo, rsFirst, rsSize, sucessMethod, failMethod) => {
		let self = this
		this.getServiceUrl((serviceUrl) => {
			return axios.get(serviceUrl + 'orgunits', {
				headers: {
					'Access-Control-Allow-Origin': '*',
					'Content-Type': 'application/json'
				},
				params: {
					"lang": lang,
					"rs-size": rsSize,
					"rs-first": rsFirst,
					"search-term": searchTerm,
					"guide-num": guideNumber,
					"ou-type": outType,
					"ext": extInfo

				}
			}).then(sucessMethod).catch(function(error) {
				self.serviceDiscovery.handleRESTError(error, failMethod)
			})
		})
	}

	getDOIDomainData = (lang, domainName, sucessMethod, failMethod) => {
		let self = this
		this.getDOIServiceUrl((serviceUrl) => {
			return axios.get(serviceUrl + 'domains/' + domainName, {
				headers: {
					'Access-Control-Allow-Origin': '*',
					'Content-Type': 'application/json'
				},
				params: {
					'lang': lang
				}
			}).then(sucessMethod).catch(function(error) {
				self.serviceDiscovery.handleRESTError(error, failMethod)
			})
		})
	}	
}
