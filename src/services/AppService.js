import Logger from 'js-logger'
import axios from 'axios'
import serviceDiscovery from '@/utils/ServiceDiscovery'

export function registerErrorHandler(appMain) {
	if (process.env.VUE_APP_VERSION != 'dev') {
		appMain.config.errorHandler = function(err) {
			appService.log({
				severity: 'error',
				timestamp: new Date().getTime(),
				column: err.columnNumber,
				line: err.lineNumber,
				message: err.message,
				stacktrace: err.stack,
				client: document.documentURI,
				browser: navigator.userAgent,
				version: process.env.VUE_APP_VERSION + ', ' + new Date(parseInt(process.env.VUE_APP_BUILDDATE))
			})
		}
		window.onerror = function(msg, url, line, col, error) {
			appService.log({
				severity: 'error',
				timestamp: new Date().getTime(),
				column: col,
				line: line,
				message: msg,
				stacktrace: error.stack,
				client: document.documentURI,
				browser: navigator.userAgent,
				version: process.env.VUE_APP_VERSION + ', ' + new Date(parseInt(process.env.VUE_APP_BUILDDATE))
			})

			return true;
		}
	}
}

window.onunload = function() {
	appService.flush()
}

export default class AppService {
	constructor() {
		this.serviceDiscovery = serviceDiscovery
	}

	whoAmI = (sucessMethod, failMethod) => {
		let self = this
		return axios.get(serviceDiscovery.getAppServicesUrl() + 'app/whoami', {
			headers: {
				'Access-Control-Allow-Origin': '*',
				'Content-Type': 'application/json'
			}
		}).then(sucessMethod).catch(function(error) {
			self.serviceDiscovery.handleRESTError(error, failMethod)
		})
	}

	log = (msg) => {
		Logger.info('Logging exception: ', msg)

		return axios.post(serviceDiscovery.getAppServicesUrl() + 'app/log', msg, {
			headers: {
				'Access-Control-Allow-Origin': '*',
				'Content-Type': 'application/json'
			}
		})
	}

	flush = () => {
		Logger.info('Flushed exception log.')
	}

	getHelloUrl = () => {
		return serviceDiscovery.getAppServicesUrl() + 'hello'
	}
}

const appService = new AppService()
