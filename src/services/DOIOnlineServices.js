import axios from 'axios'
import serviceDiscovery from '@/utils/ServiceDiscovery'

export default class DOIOnlineServices {
	constructor() {
		this.serviceDiscovery = serviceDiscovery
	}

	getServiceUrl = ((serviceCall) => {
		this.serviceDiscovery.getDOIOnlineServicesUrl().then((serviceUrl) => {
			serviceCall(serviceUrl)
		})
	})

	searchDOI = (doiSucheIN, sucessMethod, failMethod) => {
		let self = this
		this.getServiceUrl((serviceUrl) => {
			return axios.post(serviceUrl + 'dois/search', doiSucheIN, {
				headers: {
					'Access-Control-Allow-Origin': '*',
					'Content-Type': 'application/json'
				},
			}).then(sucessMethod).catch(function(error) {
				self.serviceDiscovery.handleRESTError(error, failMethod)
			})
		})
	}

	searchError = (errorSucheIN, sucessMethod, failMethod) => {
		let self = this
		this.getServiceUrl((serviceUrl) => {
			return axios.post(serviceUrl + 'errors/search', errorSucheIN, {
				headers: {
					'Access-Control-Allow-Origin': '*',
					'Content-Type': 'application/json'
				},
			}).then(sucessMethod).catch(function(error) {
				self.serviceDiscovery.handleRESTError(error, failMethod)
			})
		})
	}

	searchUser = (userSucheIN, sucessMethod, failMethod) => {
		let self = this
		this.getServiceUrl((serviceUrl) => {
			return axios.post(serviceUrl + 'users/search', userSucheIN, {
				headers: {
					'Access-Control-Allow-Origin': '*',
					'Content-Type': 'application/json'
				},
			}).then(sucessMethod).catch(function(error) {
				self.serviceDiscovery.handleRESTError(error, failMethod)
			})
		})
	}

	searchPool = (poolSucheIN, sucessMethod, failMethod) => {
		let self = this
		this.getServiceUrl((serviceUrl) => {
			return axios.post(serviceUrl + 'pools/search', poolSucheIN, {
				headers: {
					'Access-Control-Allow-Origin': '*',
					'Content-Type': 'application/json'
				},
			}).then(sucessMethod).catch(function(error) {
				self.serviceDiscovery.handleRESTError(error, failMethod)
			})
		})
	}

	getPool = (poolId, sucessMethod, failMethod) => {
		let self = this
		this.getServiceUrl((serviceUrl) => {
			return axios.get(serviceUrl + 'pools/' + poolId, {
				headers: {
					'Access-Control-Allow-Origin': '*',
					'Content-Type': 'application/json'
				},
			}).then(sucessMethod).catch(function(error) {
				self.serviceDiscovery.handleRESTError(error, failMethod)
			})
		})
	}

	getDashboard = (sucessMethod, failMethod) => {
		let self = this
		this.getServiceUrl((serviceUrl) => {
			return axios.get(serviceUrl + 'pools/dashboard', {
				headers: {
					'Access-Control-Allow-Origin': '*',
					'Content-Type': 'application/json'
				},
			}).then(sucessMethod).catch(function(error) {
				self.serviceDiscovery.handleRESTError(error, failMethod)
			})
		})
	}

	streamDashboard = (evenListenerMethod) => {
		this.getServiceUrl((serviceUrl) => {
			let evtSource = new EventSource(serviceUrl + 'pools/dashboard/stream')

			evtSource.addEventListener('refresh-dashboard-status', function(evt) {
				evenListenerMethod(evtSource, JSON.parse(evt.data), false)
			})
			
			evtSource.addEventListener('close', function(evt) {
				evenListenerMethod(null, JSON.parse(evt.data), true)
				evtSource.close() 
			})

			return evtSource
		})
    }

	createPool = (poolIN, sucessMethod, failMethod) => {
		let self = this
		this.getServiceUrl((serviceUrl) => {
			return axios.post(serviceUrl + 'pools/', poolIN, {
				headers: {
					'Access-Control-Allow-Origin': '*',
					'Content-Type': 'application/json'
				},
			}).then(sucessMethod).catch(function(error) {
				self.serviceDiscovery.handleRESTError(error, failMethod)
			})
		})
	}

	updatePool = (poolId, poolIN, sucessMethod, failMethod) => {
		let self = this
		this.getServiceUrl((serviceUrl) => {
			return axios.put(serviceUrl + 'pools/' + poolId, poolIN, {
				headers: {
					'Access-Control-Allow-Origin': '*',
					'Content-Type': 'application/json'
				},
			}).then(sucessMethod).catch(function(error) {
				self.serviceDiscovery.handleRESTError(error, failMethod)
			})
		})
	}

	deletePools = (idList, sucessMethod, failMethod) => {
		let self = this
		this.getServiceUrl((serviceUrl) => {
			return axios.delete(serviceUrl + 'pools', {
				headers: {
					'Access-Control-Allow-Origin': '*',
					'Content-Type': 'application/json'
				},
				params: {
					'ids': idList.join(',')
				}
			}).then(sucessMethod).catch(function(error) {
				self.serviceDiscovery.handleRESTError(error, failMethod)
			})
		})
	}

	enablePools = (idList, sucessMethod, failMethod) => {
		let self = this
		this.getServiceUrl((serviceUrl) => {
			return axios.put(serviceUrl + 'pools/enable', idList, {
				headers: {
					'Access-Control-Allow-Origin': '*',
					'Content-Type': 'application/json'
				}
			}).then(sucessMethod).catch(function(error) {
				self.serviceDiscovery.handleRESTError(error, failMethod)
			})
		})
	}

	disablePools = (idList, sucessMethod, failMethod) => {
		let self = this
		this.getServiceUrl((serviceUrl) => {
			return axios.put(serviceUrl + 'pools/disable', idList, {
				headers: {
					'Access-Control-Allow-Origin': '*',
					'Content-Type': 'application/json'
				}
			}).then(sucessMethod).catch(function(error) {
				self.serviceDiscovery.handleRESTError(error, failMethod)
			})
		})
	}

	getError = (errorId, sucessMethod, failMethod) => {
		let self = this
		this.getServiceUrl((serviceUrl) => {
			return axios.get(serviceUrl + 'errors/' + errorId, {
				headers: {
					'Access-Control-Allow-Origin': '*',
					'Content-Type': 'application/json'
				},
			}).then(sucessMethod).catch(function(error) {
				self.serviceDiscovery.handleRESTError(error, failMethod)
			})
		})
	}
	
	updateError = (errorId, errorIN, sucessMethod, failMethod) => {
		let self = this
		this.getServiceUrl((serviceUrl) => {
			return axios.put(serviceUrl + 'errors/' + errorId, errorIN, {
				headers: {
					'Access-Control-Allow-Origin': '*',
					'Content-Type': 'application/json'
				},
			}).then(sucessMethod).catch(function(error) {
				self.serviceDiscovery.handleRESTError(error, failMethod)
			})
		})
	}

	getUser = (userId, sucessMethod, failMethod) => {
		let self = this
		this.getServiceUrl((serviceUrl) => {
			return axios.get(serviceUrl + 'users/' + userId, {
				headers: {
					'Access-Control-Allow-Origin': '*',
					'Content-Type': 'application/json'
				},
			}).then(sucessMethod).catch(function(error) {
				self.serviceDiscovery.handleRESTError(error, failMethod)
			})
		})
	}

	createUser = (userIN, sucessMethod, failMethod) => {
		let self = this
		this.getServiceUrl((serviceUrl) => {
			return axios.post(serviceUrl + 'users/', userIN, {
				headers: {
					'Access-Control-Allow-Origin': '*',
					'Content-Type': 'application/json'
				},
			}).then(sucessMethod).catch(function(error) {
				self.serviceDiscovery.handleRESTError(error, failMethod)
			})
		})
	}

	updateUser = (userId, userIN, sucessMethod, failMethod) => {
		let self = this
		this.getServiceUrl((serviceUrl) => {
			return axios.put(serviceUrl + 'users/' + userId, userIN, {
				headers: {
					'Access-Control-Allow-Origin': '*',
					'Content-Type': 'application/json'
				},
			}).then(sucessMethod).catch(function(error) {
				self.serviceDiscovery.handleRESTError(error, failMethod)
			})
		})
	}

	deleteUsers = (idList, sucessMethod, failMethod) => {
		let self = this
		this.getServiceUrl((serviceUrl) => {
			return axios.delete(serviceUrl + 'users', {
				headers: {
					'Access-Control-Allow-Origin': '*',
					'Content-Type': 'application/json'
				},
				params: {
					'ids': idList.join(',')
				}
			}).then(sucessMethod).catch(function(error) {
				self.serviceDiscovery.handleRESTError(error, failMethod)
			})
		})
	}

	getDOI = (doiId, sucessMethod, failMethod) => {
		let self = this
		this.getServiceUrl((serviceUrl) => {
			return axios.get(serviceUrl + 'dois/' + doiId, {
				headers: {
					'Access-Control-Allow-Origin': '*',
					'Content-Type': 'application/json'
				},
			}).then(sucessMethod).catch(function(error) {
				self.serviceDiscovery.handleRESTError(error, failMethod)
			})
		})
	}

	createDOI = (doiIN, sucessMethod, failMethod) => {
		let self = this
		this.getServiceUrl((serviceUrl) => {
			return axios.post(serviceUrl + 'pools/' + doiIN['pool-id'] + '/dois', doiIN, {
				headers: {
					'Access-Control-Allow-Origin': '*',
					'Content-Type': 'application/json'
				},
			}).then(sucessMethod).catch(function(error) {
				self.serviceDiscovery.handleRESTError(error, failMethod)
			})
		})
	}

	updateDOI = (doiId, doiIN, sucessMethod, failMethod) => {
		let self = this
		this.getServiceUrl((serviceUrl) => {
			return axios.put(serviceUrl + 'dois/' + doiId, doiIN, {
				headers: {
					'Access-Control-Allow-Origin': '*',
					'Content-Type': 'application/json'
				},
			}).then(sucessMethod).catch(function(error) {
				self.serviceDiscovery.handleRESTError(error, failMethod)
			})
		})
	}

	deleteDOI = (doiId, sucessMethod, failMethod) => {
		let self = this
		this.getServiceUrl((serviceUrl) => {
			return axios.delete(serviceUrl + 'dois/' + doiId, {
				headers: {
					'Access-Control-Allow-Origin': '*',
					'Content-Type': 'application/json'
				},
			}).then(sucessMethod).catch(function(error) {
				self.serviceDiscovery.handleRESTError(error, failMethod)
			})
		})
	}

	deleteDOIs = (idList, sucessMethod, failMethod) => {
		let self = this
		this.getServiceUrl((serviceUrl) => {
			return axios.delete(serviceUrl + 'dois', {
				headers: {
					'Access-Control-Allow-Origin': '*',
					'Content-Type': 'application/json'
				},
				params: {
					'ids': idList.join(',')
				}
			}).then(sucessMethod).catch(function(error) {
				self.serviceDiscovery.handleRESTError(error, failMethod)
			})
		})
	}

	getDOIHistory = (doiId, sucessMethod, failMethod) => {
		let self = this
		this.getServiceUrl((serviceUrl) => {
			return axios.get(serviceUrl + 'dois/' + doiId + '/history', {
				headers: {
					'Access-Control-Allow-Origin': '*',
					'Content-Type': 'application/json'
				},
			}).then(sucessMethod).catch(function(error) {
				self.serviceDiscovery.handleRESTError(error, failMethod)
			})
		})
	}

	startFullImport = (poolId, sucessMethod, failMethod) => {
		let self = this
		this.getServiceUrl((serviceUrl) => {
			return axios.put(serviceUrl + 'pools/' + poolId + '/fullimport', {
				headers: {
					'Access-Control-Allow-Origin': '*',
					'Content-Type': 'application/json'
				},
			}).then(sucessMethod).catch(function(error) {
				self.serviceDiscovery.handleRESTError(error, failMethod)
			})
		})
	}
	
	startImport = (poolId, sucessMethod, failMethod) => {
		let self = this
		this.getServiceUrl((serviceUrl) => {
			return axios.put(serviceUrl + 'pools/' + poolId + '/import', {
				headers: {
					'Access-Control-Allow-Origin': '*',
					'Content-Type': 'application/json'
				},
			}).then(sucessMethod).catch(function(error) {
				self.serviceDiscovery.handleRESTError(error, failMethod)
			})
		})
	}
	
	startExport = (poolId, sucessMethod, failMethod) => {
		let self = this
		this.getServiceUrl((serviceUrl) => {
			return axios.put(serviceUrl + 'pools/' + poolId + '/export', {
				headers: {
					'Access-Control-Allow-Origin': '*',
					'Content-Type': 'application/json'
				},
			}).then(sucessMethod).catch(function(error) {
				self.serviceDiscovery.handleRESTError(error, failMethod)
			})
		})
	}

	startFullExport = (poolId, sucessMethod, failMethod) => {
		let self = this
		this.getServiceUrl((serviceUrl) => {
			return axios.put(serviceUrl + 'pools/' + poolId + '/fullexport', {
				headers: {
					'Access-Control-Allow-Origin': '*',
					'Content-Type': 'application/json'
				},
			}).then(sucessMethod).catch(function(error) {
				self.serviceDiscovery.handleRESTError(error, failMethod)
			})
		})
	}
		
	startClear = (poolId, sucessMethod, failMethod) => {
		let self = this
		this.getServiceUrl((serviceUrl) => {
			return axios.put(serviceUrl + 'pools/' + poolId + '/clear', {
				headers: {
					'Access-Control-Allow-Origin': '*',
					'Content-Type': 'application/json'
				},
			}).then(sucessMethod).catch(function(error) {
				self.serviceDiscovery.handleRESTError(error, failMethod)
			})
		})
	}

	startUpdate = (poolId, sucessMethod, failMethod) => {
		let self = this
		this.getServiceUrl((serviceUrl) => {
			return axios.put(serviceUrl + 'pools/' + poolId + '/update', {
				headers: {
					'Access-Control-Allow-Origin': '*',
					'Content-Type': 'application/json'
				},
			}).then(sucessMethod).catch(function(error) {
				self.serviceDiscovery.handleRESTError(error, failMethod)
			})
		})
	}
}
