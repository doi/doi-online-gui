import {createRouter, createWebHistory } from 'vue-router'

// In some cases, the 'back' function caused a
// chunk loading error. This is the workaround!
// https://forum.vuejs.org/t/need-help-with-vue-router-lazy-loading-chunk-error/21652/4
//const Index = () => import('@/views/Index/Index')
import Index from '@/views/Index/Index'
const Imprint = () => import('@/components/fw/Imprint')
const ListDOIs = () => import('@/views/ListDOIs/ListDOIs')
const ListRepos = () => import('@/views/ListRepos/ListRepos')
const ListUsers = () => import('@/views/ListUsers/ListUsers')
const ListErrors = () => import('@/views/ListErrors/ListErrors')
const ShowDOIDetails = () => import('@/views/ShowDOIDetails/ShowDOIDetails')
const ShowRepoDetails = () => import('@/views/ShowRepoDetails/ShowRepoDetails')
const ShowUserDetails = () => import('@/views/ShowUserDetails/ShowUserDetails')
const ShowErrorDetails = () => import('@/views/ShowErrorDetails/ShowErrorDetails')
const ShowDOIHistory = () => import('@/views/ShowDOIHistory/ShowDOIHistory')

export default createRouter({
	history: createWebHistory(window.location.pathname.substring(0, window.location.pathname.lastIndexOf("/"))),
	
	scrollBehavior(to, from, savedPosition) {
		if (savedPosition) {
			return savedPosition
		} else {
			return { x: 0, y: 0 }
		}
	},
	
	base: window.location.pathname.substring(0, window.location.pathname.lastIndexOf("/")),
	
	routes: [
		{
			path: '/:pathMatch(.*)*',
			redirect: '/'
		},
		{
			path: '/',
			component: Index,
			name: 'index'
		},
		{
			path: '/imprint.view',
			component: Imprint,
			name: 'imprint'
		},
		{
			path: '/listdois.view',
			component: ListDOIs,
			name: 'listdois',
			props: route => ({
				from: route.query.from,
				to: route.query.to,
				modstart: route.query.modstart,
				modend: route.query.modend,
				doi: route.query.doi,
				url: route.query.url,
				repoId: route.query.repoId
			})
		},
		{
			path: '/listrepos.view',
			component: ListRepos,
			name: 'listrepos',
			props: route => ({
				name: route.query.name,
				doiPrefix: route.query.doiPrefix,
				urlPrefix: route.query.urlPrefix,
				username: route.query.username
			})
		},
		{
			path: '/listusers.view',
			component: ListUsers,
			name: 'listusers',
			props: route => ({
				firstname: route.query.firstname,
				lastname: route.query.lastname,
				uniqueId: route.query.uniqueId,
				institution: route.query.institution,
				admin: route.query.admin,
				repoId: route.query.repoId
			})
		},
		{
			path: '/listerrors.view',
			component: ListErrors,
			name: 'listerrors',
			props: route => ({
				from: route.query.from,
				to:route.query.to,
				msg: route.query.msg,
				code: route.query.code,
				handled: route.query.handled,
				repoId: route.query.repoId,
				doi: route.query.doi
			})
		},
		{
			path: '/doi.view',
			component: ShowDOIDetails,
			name: 'doi',
			props: route => ({
				id: route.query.id
			})
		},
		{
			path: '/repo.view',
			component: ShowRepoDetails,
			name: 'repo',
			props: route => ({
				id: route.query.id
			})
		},
		{
			path: '/user.view',
			component: ShowUserDetails,
			name: 'user',
			props: route => ({
				id: route.query.id
			})
		},
		{
			path: '/error.view',
			component: ShowErrorDetails,
			name: 'error',
			props: route => ({
				id: route.query.id
			})
		},
		{
			path: '/doihistory.view',
			component: ShowDOIHistory,
			name: 'doihistory',
			props: route => ({
				id: route.query.id
			})
		},
	]
})
