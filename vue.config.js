const path = require('path')
let indexPage = ''

if (process.env.version) {
	// Variable is set, so it's a maven build
	process.env.VUE_APP_VERSION = process.env.version
	process.env.VUE_APP_STAGE = process.env.testStage
	process.env.VUE_APP_BUILDNUMBER = process.env.buildNumber
	process.env.VUE_APP_BRANCH = process.env.scmBranch
	process.env.NODE_ENV = 'production'

	indexPage = 'WEB-INF/views/index.jsp'
} else {
	// Variable not set, so it's a devserver build
	process.env.VUE_APP_VERSION = 'dev'
	process.env.VUE_APP_STAGE = 'jsdev'
	process.env.VUE_APP_BUILDNUMBER = 'local'
	process.env.VUE_APP_BRANCH = '?'
	
	indexPage = 'index.html'
}

process.env.VUE_APP_CALL_WHOAMI = true
process.env.VUE_APP_BUILDDATE = new Date().getTime()
process.env.VUE_APP_CLIENT_ID = 'doi-online-gui'

const selectedAccount = 0
const testAccount = [{
	// #0: Admin
	uniqueId: '234601@ethz.ch',
	givenName: 'Claire',
	surname: 'Läubli',
	uid: 't234601'
}, {
	// #1: User
	uniqueId: '262321@vho-switchaai.ch',
	givenName: 'Samantha',
	surname: 'Foulger_VHO',
	uid: 't1811747'
}]

module.exports = {
	publicPath: './',

	pages: {
		'index': {
			entry: './src/main.js',
			template: 'public/template.html',
			filename: indexPage,
			title: 'DOI Dashboard',
			chunks: ['chunk-vendors', 'chunk-common', 'index']
		}
	},

	outputDir: path.resolve(__dirname, './target/' + process.env.targetDir
		+ '/'),

	devServer: {
		compress: false,
		port: 8080,
		clientLogLevel: 'info',
		overlay: true,
		open: 'Firefox',
		openPage: '/',
		proxy: {
			// In JS-DEV mode, all configurations from the discovery service are ignored and
			// hardcoded to '[js-serving-host:port]/[service-name]'!

			// This proxy mapping is for the application services (discover, whoami, log)
			'/app-services-v1': {
				// Use this target for JS-DEV->ENTW scenario:
				// target: 'http://ois-dev-red1.ethz.ch:7081/doi-online-servicesEntw14/services/v1/',
				// Use this target for JS-DEV->DEV scenario:
				target: 'http://localhost:7001/webappDev0315/services/v1/',

				secure: false,
				logLevel: 'debug',
				pathRewrite: { '^/app-services-v1': '' },
				onProxyReq: function(proxyReq, req, res) {
					// Fake identity with the given uniqueID here:
					proxyReq.setHeader('uniqueID', testAccount[selectedAccount].uniqueId);
					proxyReq.setHeader('givenName', testAccount[selectedAccount].givenName);
					proxyReq.setHeader('surname', testAccount[selectedAccount].surname);
					proxyReq.setHeader('uid', testAccount[selectedAccount].uid);
				}
			},
			// This proxy mapping is for accessing jobs services in JS-DEV
			// scenario.
			'/doi-online-services-v1': {
				// Use this target for JS-DEV->ENTW scenario:
				// target: 'http://ois-dev-red1.ethz.ch:7081/doi-online-servicesEntw14/services/v1',
				// Use this target for JS-DEV->DEV scenario:
				target: 'http://localhost:7001/webappDev0315/services/v1',

				secure: false,
				logLevel: 'debug',
				pathRewrite: { '^/doi-online-services-v1': '' },
				onProxyReq: function(proxyReq, req, res) {
					proxyReq.setHeader('uniqueID', testAccount[selectedAccount].uniqueId);
					proxyReq.setHeader('givenName', testAccount[selectedAccount].givenName);
					proxyReq.setHeader('surname', testAccount[selectedAccount].surname);
					proxyReq.setHeader('uid', testAccount[selectedAccount].uid);
				}
			}
		}
	},
	
	productionSourceMap: false,
	
	configureWebpack: {
		devtool: 'source-map'
	},

	chainWebpack: config => {
		config.module
			.rule('vue')
			.use('vue-loader')
			.tap(options => ({
				compilerOptions: {
					isCustomElement: tag => tag.startsWith('duet-')
				}
			})
		)
	}
}
